const chai = require('chai');
const chaiThings = require('chai-things');
const { fibonacci } = require('../app');

const { should, expect, assert } = chai;
chai.use(chaiThings);

describe('fibonacii normal test cases', () => {
  it('should be a number', () => {
    expect(fibonacci(20)).to.be.a('number');
  });

  it('should be 1 when num is 1', () => {
    expect(fibonacci(1)).to.be.equal(1);
  });

  it('should be 2 when num is 3', () => {
    expect(fibonacci(3)).to.be.equal(2);
  });
});

describe('fibonacci error test case', () => {
  it('should throw an error when num is not a number', () => {
    expect(() => {
      fibonacci('');
    }).to.throw(Error);
  });

  it('should throw an error when num is less than 1', () => {
    expect(() => {
      fibonacci(-2);
    }).to.throw(Error);
  });
});

describe('fibonacci time complexity', () => {
  let t1 = Date.now();

  fibonacci(100);

  let t2 = Date.now();
  let time = t2 - t1;

  it('Should have O(n) time complexity', () => {
    expect(time).to.be.below(100);
  });
});
