module.exports = {
  fibonacci: (num) => {
    if (typeof num === 'number' && num > 0) {
      let temp1 = 0;
      let temp2 = 1;
      let temp3;

      if (num == 1) {
        return temp1 + temp2;
      }
      for (let i = 1; i < num; i++) {
        temp3 = temp1 + temp2;
        temp1 = temp2;
        temp2 = temp3;
      }
      return temp3;
    } else {
      throw new Error('Invalid Input');
    }
  },
};
